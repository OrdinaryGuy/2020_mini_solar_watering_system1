//------------------------- User Interface ---------------------------
unsigned long pushTime;
unsigned long pumpTime;
bool blinkFlag;
bool buttonFlag = true;
bool LEDstate;
byte mode = 0;


void userInterface() {
  // Go to settings after waking up when button is pressed
  if (digitalRead(button) == HIGH) {

    // Before going to settings system chack voltage. When low, skip user interface 
    getVoltage(20);
    if (currentVoltage < minOperationalVoltage) {
      for (int i = 0; i < 20; i++){
        digitalWrite(red, HIGH);
        delay(10);
        digitalWrite(red, LOW);
        delay(10);
      }
      lowPower = true;
      goto setOut;
    }
    
    // Sensor error check. Sensor needs minimum voltage for correct results
    if (sensErrorFlag == true || currentVoltage < minSensVoltage) {
      sensErrorFlag = false;
      for (int i = 0; i < 20; i++){
        digitalWrite(green, HIGH);
        delay(10);
        digitalWrite(green, LOW);
        delay(10);
      }
    }
  
    // Automatic watering check
    if (autoWateringFlag == true) {
      autoWateringFlag = false;
      for (int i = 0; i < 20; i++){
        digitalWrite(blue, HIGH);
        delay(10);
        digitalWrite(blue, LOW);
        delay(10);
      }
    }
    
    introAnimation(20);
    
    // Initialize while condition
    pushTime = millis();

    //---------------- Main part of device's settings --------------------
    // after specific time of inactivity goes out of loop
    while (millis() - pushTime < 5 * 250 || digitalRead(button) == HIGH) {

      progButton();
      
      // LEDs animation during doing nothing or short press
      if (digitalRead(button) == LOW || (digitalRead(button) == HIGH && millis() - pushTime < 50)) {
        blinking(40);
      }
      else if (digitalRead(button) == HIGH && buttonFlag == false) {
        
        // Transition animation between short and long press
        if (millis() - pushTime >= 50 && millis() - pushTime < 250) {
          blinking(10);
        }
        
        // Long push, recording sensor treshold value
        else if (millis() - pushTime >= 250) {
          if (mode == 0) {
            digitalWrite(green, HIGH);
            while (digitalRead(button) == HIGH) {}
            getSoilMoisture(30);
            delay(70);
            allLEDanimation(5, 10);
            pushTime = millis();
    
            // Flag that program had been modified
            setActive = true;
            if (currentHumidity > minSensValue && currentHumidity < maxSensValue) {
              sensTreshold = currentHumidity;
              sensChange = true;
            }
          }
  
          // Long push, recording duration of watering
          else if (mode == 1) {
            digitalWrite(blue, HIGH);
            while (digitalRead(button) == HIGH) {} // Holds the program until button is released
            digitalWrite(blue, LOW);
            delay(20); // Debouncing delay
            runModeControl();
            allLEDanimation(5, 10);
        
            // Flags
            setActive = true;
            progWatering = true;
            
            // Value is recorded only if process was uninterrupted
            if (lowPower == false) {
              // Recorded value
              wateringTime = (millis() - pumpTime)/250;
              timeChange = true;
            }
          }
  
          // Long push, extra watering
          else if (mode == 2) {
            digitalWrite(red, HIGH);
            while (digitalRead(button) == HIGH) {} // Holds the program until button is released
            digitalWrite(blue, HIGH);
            delay(20); // Debouncing delay
            
            runModeControl();
            allLEDanimation(5, 10);
  
            // Flags
            setActive = true;
            manWatering = true;
          }
        }
      }
    }

    // Saving new setData
    writeSetData();
    allLEDanimation(8, 40);
  }
  setOut:;
}

void progButton() {
  if (digitalRead(button) == HIGH && buttonFlag == true) {
    buttonFlag = false;
    pushTime = millis();
    delay(10);
  }
  
  else if (digitalRead(button) == LOW && buttonFlag == false) {
    if (millis() - pushTime < 50) {
      mode++;
      if (mode > 2) {mode = 0;}
    }
    buttonFlag = true;
    pushTime = millis();
    delay(10);
  }
}

void runModeControl() {
  digitalWrite(boost, HIGH);
  pumpTime = millis();

  // Loop waits until button is pressed again or time passes 255s or voltage decreases too low
  byte lowVoltage = 0;
  while (digitalRead(button) == LOW) {
    getVoltage(5);
    if (currentVoltage < shutDownVoltage && millis() - pumpTime > 250) {
      lowVoltage++;
    }
    else {
      lowVoltage = 0;
    }
    if (lowVoltage > 4) {
      lowPower = true;
      break;
    }
    else if (((millis() - pumpTime)/250 >= 255)) {
      break;
    }
    
    if (mode == 1) {
      recordingAnimation();
    }
  }
  digitalWrite(boost, LOW);
  pushTime = millis();
  
  // preparing for next watering
  wateringFlag = false;
  unlockWatering = false;
  wateringEnable = 0;
  timeToWater = wakeUpCount + minWaterPeriod; 
}

// Function checks basic parameters before starting the program
void initialCheckInfo() {
  if (lowPower == true) {
    for (int i = 0; i < 20; i++) {
      digitalWrite(red, HIGH);
      delay(10);
      digitalWrite(red, LOW);
      delay(10);
    }
  goto outOfFunction;
  }
  if (sensError == true) {
    while (currentHumidity < minSensValue || currentHumidity > maxSensValue) {
      back:;
      digitalWrite(green, HIGH);
      delay(10);
      digitalWrite(green, LOW);
      getSoilMoisture(20);
      if (digitalRead(button) == HIGH) {
        break;   
      }
    }
  }
  digitalWrite(green, HIGH);
  delay(250);
  digitalWrite(green, LOW);
  getSoilMoisture(10);
  if ((currentHumidity < minSensValue || currentHumidity > maxSensValue) && digitalRead(button) != HIGH) {
    goto back;
  }
  sensErrorFlag = false;
  allLEDanimation(3, 10);
    
  int comparingVoltage = currentVoltage;
  do {
    digitalWrite (boost, HIGH);
    delay(1);
    getVoltage(20);
    digitalWrite(boost, LOW);
    if (comparingVoltage - currentVoltage <= 10) {
      digitalWrite(blue, HIGH);
      delay(10);
      digitalWrite(blue, LOW);
      delay(9);
    } 
    if (digitalRead(button) == HIGH || comparingVoltage - currentVoltage >= 10) {
      digitalWrite(blue, HIGH);
      delay(250);
      digitalWrite(blue, LOW);

      digitalWrite (boost, HIGH);
      delay(1);
      getVoltage(20);
      digitalWrite(boost, LOW);
      if (digitalRead(button) == HIGH || comparingVoltage - currentVoltage >= 10) {
        allLEDanimation(3, 10);
        break;
      }
    } 
  } while (1);
outOfFunction:;
}

//------------- LED animations --------------------

void blinking (byte tempo) {
  if (millis() % tempo == 0 && blinkFlag == true) {
    blinkFlag = false;
    if (mode == 0) {
      digitalWrite(red, LOW);
      digitalWrite(green, LEDstate);
      digitalWrite(blue, LOW);
    }
    else if (mode == 1) {
      digitalWrite(red, LOW);
      digitalWrite(green, LOW);
      digitalWrite(blue, LEDstate);
    }
    else if (mode == 2) {
      digitalWrite(red, LEDstate);
      digitalWrite(green, LOW);
      digitalWrite(blue, LOW);
    }
    LEDstate = !LEDstate;
  }
  else if (millis() % tempo != 0 && blinkFlag == false) {
    blinkFlag = true;
  }
}

void introAnimation(byte pace) {
  byte animation[] = {6, 5, 4, 6, 5, 4, 6, 5, 4, 6, 5, 4, 6, 5, 4, 6, 5, 4, 6, 5, 4, 6, 5, 4};
  for(int i = 0; i < sizeof(animation); i++) {
      digitalWrite(animation[i], HIGH);
      delay(pace);
      digitalWrite(animation[i], LOW);
  }
}

void allLEDanimation(byte number, byte duration) {
  for (int i = 0; i < number; i++) {
    digitalWrite(red, HIGH);
    digitalWrite(green, HIGH);
    digitalWrite(blue, HIGH);
    delay(duration);
    digitalWrite(red, LOW);
    digitalWrite(green, LOW);
    digitalWrite(blue, LOW);
    delay(duration);
  }
}

void recordingAnimation() {
  digitalWrite(blue, HIGH);
  if (millis() % 125 == 0 && blinkFlag == false) {
    blinkFlag = true;
    digitalWrite(green, LEDstate);
    LEDstate = !LEDstate;
    digitalWrite(red, LEDstate);
  }
  else if (millis() % 125 != 0 && blinkFlag == true) {
    blinkFlag = false;
  }
}
