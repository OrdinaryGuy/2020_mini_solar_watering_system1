#include <EEPROM.h>

// User changeable constants (54 wakups/minute for my Adruino board)
const uint16_t measurePeriod = 540; // 10 minutes
const uint32_t minWaterPeriod = 25920; // 8 hours
const uint32_t maxWaterPeriod = 77760; // 1 day
const uint32_t savePeriod = 12960; // 4 hours

const int stopChargingVoltage = 700; // 166*voltage should be 4.2V
const int startChargingVoltage = 660; // should be 4.0V
const int shutDownVoltage = 530; // should be 3.2V System fail at 3.1V
const int minOperationalVoltage = 620; // should be 3,75V
const int minSensVoltage = 560; // should be 3,4V

const int minSensValue = 360;
const int maxSensValue = 500;

const byte wateringEnableCount = 12;

// Variables for timing
unsigned long wakeUpCount = 0;
unsigned long timeToMeasure = measurePeriod;
unsigned long timeToWater = minWaterPeriod;
unsigned long timeToSave = savePeriod;

int currentHumidity;
int currentVoltage;

byte wateringEnable = 0;

// Settable values
int sensTreshold = 400;
byte wateringTime = 60;

// Flags
bool measureFlag = true;
bool wateringFlag = false;
bool unlockWatering = false;
bool saveFlag = false;
bool sensChange = false;
bool timeChange = false;
bool chargingFlag = false;

bool solarChrg = false;
bool lowPower = false;
bool setActive = false;
bool rstActive = false;
bool sensError = false;
bool sensErrorFlag = false; // for user interface only
bool progWatering = false;
bool autoWatering = false;
bool autoWateringFlag = false; // for user interface only
bool manWatering = false;
bool ACKbit = false;

// digital outputs
#define button  2
#define red     6
#define green   5
#define blue    4
#define ldo     7
#define boost   9
#define solar   A3

// analog inputs
#define battery A4
#define sensor  A5

void setup() {
  // Slowing clockspeed to 4Mhz in order to run within specs at Li-ion battery voltage
  CLKPR = 0x80;
  CLKPR = 0x02;
  
  // Serial communication to read EEPROM content
  // Due to reduced clock speed baud rate at the pc is 38400 bps
  Serial.begin(153600);

  // Pulling all digital pins down except TX and RX pins to reduce power consumption
  for (int pin = 2; pin < 18; pin++) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }

  // Internal 1,1V reference
  analogReference(INTERNAL);

  // Flag about device reset
  rstActive = true;

  readEEPROMdata();
  //clrEEPROM();
  uploadParameters();
  getSoilMoisture(20);
  getVoltage(20);
  powerControl();
  wateringControl();
  initialCheckInfo();
  introAnimation(20);
  findMemoryLocation();
  writeData();

  // Enable WatchDog timer waking up every second
  WDTCSR = (24);//change enable and WDE - also resets
  WDTCSR = (6);//prescalers only - get rid of the WDE and WDCE bit
  WDTCSR |= (1<<6);//enable interrupt mode
  
  // Enable deep sleep mode
  SMCR |= (1 << 2); //power down mode
  SMCR |= 1;//enable sleep
}

void loop() {
  userInterface();
  processData();
  watering();
  saveData();

  delay(10);
  __asm__  __volatile__("sleep"); // in line assembler to go to sleep
}

// Watchdog timer interrupt
ISR(WDT_vect) {
  wakeUpCount++;
}

void processData() {
  if (wakeUpCount >= timeToMeasure) {
    do {
      timeToMeasure += measurePeriod;
    } while (timeToMeasure < wakeUpCount);
    
    getSoilMoisture(20);
    getVoltage(20);
    powerControl();
    wateringControl();

  //Serial.print("Soil moisture: ");
  //Serial.println(currentHumidity);
    
  //Serial.print("Voltage: ");
  //Serial.println(currentVoltage);
  }
}

void getSoilMoisture(byte numOfMeasurements) {
  unsigned int measuredValue;
  // Enable ADC
  ADCSRA |= (1 << 7);
  
  // Sensor value
  measuredValue = 0;
  digitalWrite(ldo, HIGH);
  delay(10);
  for (int i = 0; i < numOfMeasurements; i++) {
    measuredValue += analogRead(sensor);
    delayMicroseconds(10);
  }
  digitalWrite(ldo, LOW);
  currentHumidity = measuredValue / numOfMeasurements;

  // Disable ADC
  ADCSRA &= ~(1 << 7);
}

void getVoltage(byte numOfMeasurements) {
  unsigned int measuredValue;
  // Enable ADC
  ADCSRA |= (1 << 7);

  // Voltage value
  measuredValue = 0;
  for (int i = 0; i < numOfMeasurements; i++) {
    measuredValue += analogRead(battery);
    delayMicroseconds(10);
  }
  currentVoltage = measuredValue / numOfMeasurements;

  // Disable ADC
  ADCSRA &= ~(1 << 7);
}

void powerControl(){
  if ((currentVoltage <= startChargingVoltage && chargingFlag == false) || rstActive == true){
    digitalWrite(solar, HIGH);
    chargingFlag = true;
  }
  else if (currentVoltage >= stopChargingVoltage && chargingFlag == true) {
    digitalWrite(solar, LOW);
    chargingFlag = false;
  }

  if (chargingFlag == true && solarChrg == false) {
    solarChrg = true;
  }
  if (currentVoltage <= minOperationalVoltage) {
    lowPower = true;
  }
}

void wateringControl() {
  
  // Sensor need minimum voltage for correct results
  if (currentVoltage > minSensVoltage) {
    // Function enables watering when sensor value is within specific range and higher than treshold value 8 times in a row
    if (currentHumidity < minSensValue || currentHumidity > maxSensValue) { // Sensor problem
      wateringEnable = 0;
      unlockWatering = false;
      sensError = true;
      sensErrorFlag = true;
    }
    else if (currentHumidity > sensTreshold) { // Soil dry
      if (wateringEnable < wateringEnableCount) {
        wateringEnable++;
      }
      //Serial.print("watering enabled = ");
      //Serial.println(wateringEnable);
    }
    else { // Soil moistured
      //Serial.println("watering anullated");
      wateringEnable = 0;
      unlockWatering = false;
    }
  }

  // Unlocking watering proces after specific time of soil being dry enough
  if (wateringEnable == wateringEnableCount && unlockWatering == false) {
    unlockWatering = true;
    //Serial.println("watering unlocked");
  }
}

void watering() {
  
  // Enables watering function to run in normal mode
  if (wakeUpCount >= timeToWater && unlockWatering == true) {
    wateringFlag = true;
    progWatering = true;
  }
  // Enables watering function to run in automatic mode
  else if (wakeUpCount >= timeToWater + (maxWaterPeriod - minWaterPeriod)) {
    unlockWatering = true;
    wateringFlag = true;
    autoWatering = true;
    autoWateringFlag = true; // This is for user interface
  }
  
  // Non-manual watering
  if (unlockWatering == true && wateringFlag == true && currentVoltage > minOperationalVoltage) {
    wateringFlag = false;
    unlockWatering = false;
    wateringEnable = 0;
    
    digitalWrite(boost, HIGH);
    digitalWrite(blue, HIGH);
    unsigned long runTime = millis();
    byte lowVoltage;
    while(millis() < runTime + (wateringTime * 250)) {
      
      // Load tend to lower voltage in circuit for a brief moment. This function should prevent this
      getVoltage(20);
      if (currentVoltage < shutDownVoltage && millis() - runTime > 250) {
        lowPower = true;
        break;
      }
      
      // Watering can be also stopped manually
      if (digitalRead(button) == HIGH) {
        break;
      }
    }
    digitalWrite(boost, LOW);
    digitalWrite(blue, LOW);
    //Serial.println("watering complete");

    timeToWater = wakeUpCount + minWaterPeriod;
  }
}
