int dataMemAddress;

void uploadParameters() {
  sensTreshold = EEPROM.read(0) + minSensValue;
  wateringTime = EEPROM.read(1);
}

void saveData() {
  if (wakeUpCount >= timeToSave) {
    do {
      timeToSave += savePeriod;
    } while (timeToSave < wakeUpCount);
    findMemoryLocation();
    writeData();
  }
}

void findMemoryLocation() {
  byte data;
  if (rstActive == true) {
    dataMemAddress = 10;
    do {
      data = EEPROM.read(dataMemAddress);
      if (data == 255) {
        break;
      }
      else if (bitRead(data, 7) == 0) {
        bitSet(data, 7);
        EEPROM.write(dataMemAddress, data);
        dataMemAddress += 3;
        if (dataMemAddress >= EEPROM.length()) {
          dataMemAddress = 10;
        }
        break;
      }
      else {
        dataMemAddress += 3;
      }
    } while (dataMemAddress < EEPROM.length());
  }
  else {
    data = EEPROM.read(dataMemAddress);
    bitSet(data, 7);
    EEPROM.write(dataMemAddress, data);
    dataMemAddress += 3;
    if (dataMemAddress >= EEPROM.length()) {
      dataMemAddress = 10;
    }
  }
  
}

void writeData() {
  byte memoryByte;
  
  // ACK + humidity
  memoryByte = 0;
  currentHumidity = constrain(currentHumidity, 160, 315);
  memoryByte = map(currentHumidity, 370, 520, 100, 0);
  bitClear(memoryByte, 7);
  EEPROM.write(dataMemAddress, memoryByte);
  
  // Voltage
  memoryByte = 0;
  memoryByte = map (currentVoltage, 465, 1023, 0,255); // values from 2.8 up to 6.16 V
  EEPROM.write(dataMemAddress + 1, memoryByte);
  
  // Alarm bits
  memoryByte = 0;
  if (solarChrg == true) {
    bitSet(memoryByte, 0);
    solarChrg = false;
  }
  if (lowPower == true) {
    bitSet(memoryByte, 1);
    lowPower = false;
  }
  if (setActive == true) {
    bitSet(memoryByte, 2);
    setActive = false;
  }
  if (rstActive == true) {
    bitSet(memoryByte, 3);
    rstActive = false;
  }
  if (sensError == true) {
    bitSet(memoryByte, 4);
    sensError = false;
  }
  if (progWatering == true) {
    bitSet(memoryByte, 5);
    progWatering = false;
  }
  if (autoWatering == true) {
    bitSet(memoryByte, 6);
    autoWatering = false;
  }
  if (manWatering == true) {
    bitSet(memoryByte, 7);
    manWatering = false;
  }
  EEPROM.write(dataMemAddress + 2, memoryByte);
}

// Writing new data after change in settings
void writeSetData() {
  byte data;
  int address;
  if (sensChange == true || timeChange == true) {
    for (address = 6; address >= 0; address -= 2) {
      EEPROM.write(address + 2, EEPROM.read(address));
      EEPROM.write(address + 3, EEPROM.read(address + 1));
    }
    if (sensChange == true) {
      sensChange == false;
      EEPROM.write(0, sensTreshold - minSensValue);
    }
    if (timeChange == true) {
      timeChange = false;
      EEPROM.write(1, wateringTime);
    }
  }
}

void readEEPROMdata(){
  int address = 0;
  int counter = 1;
  byte data;
  float voltage;
  int startingAddress;

  // Reading first 10 bytes containing settings data
  if (EEPROM.read(address) == 255) {
    Serial.println("Empty memory");
    goto nextStep;
  }
  Serial.println("No." "\t" "Treshold humidity (A.u.)" "\t" "Watering time (s)" "\n");
  while (EEPROM.read(address) == 255 || address <= 8) {
    Serial.print(counter);
    Serial.print("\t");
    Serial.print(EEPROM.read(address));
    Serial.print(" (");
    data = map((EEPROM.read(address) + minSensValue), 370, 520, 100, 0);
    Serial.print(data);
    Serial.print(" %)");
    Serial.print("\t\t\t");
    Serial.println(EEPROM.read(address + 1));
    counter++;
    address += 2;
  }
  
  nextStep:

  Serial.println("\n\n" "No." "\t" "Humidity (%)" "\t" "Voltage (V)" "\t" "Comments" "\n");
  counter = 1;
  address = 0;
  
  // Searching for last written data
  for (address = 10; address < (EEPROM.length()); address += 3) {
    data = EEPROM.read(address);
    if (bitRead(data, 7) == 0) {
      startingAddress = address + 3;
      if (startingAddress > (EEPROM.length())) {
        startingAddress = 10;
      }
      break;
    }
  }

  // Sending data through serial
  while (address != startingAddress) {
    data = EEPROM.read(address);
    //first byte cannot be 255 unless is empty
    if (data == 255) {
      break;
    }
    
    Serial.print(counter);
    Serial.print("\t");
    counter++;
    bitClear (data, 7);
    Serial.print(data);
    Serial.print("\t\t");

    // Second byte represented in volts
    data = EEPROM.read(address + 1);
    voltage = (map(data, 0, 255, 2800, 6163)) / 1000.0;
    Serial.print(voltage, 2);
    Serial.print("\t\t");

    // Third byte provides info about flags
    data = EEPROM.read(address + 2);
    if (bitRead(data, 0) == 1) {
      Serial.print("Chrg");
      Serial.print("\t");
    }
    if (bitRead(data, 1) == 1) {
      Serial.print("LPow");
      Serial.print("\t");
    }
    if (bitRead(data, 2) == 1) {
      Serial.print("Set");
      Serial.print("\t");
    }
    if (bitRead(data, 3) == 1) {
      Serial.print("Rst");
      Serial.print("\t");
    }
    if (bitRead(data, 4) == 1) {
      Serial.print("Sens");
      Serial.print("\t");
    }
    if (bitRead(data, 5) == 1) {
      Serial.print("PWat");
      Serial.print("\t");
    }
    if (bitRead(data, 6) == 1) {
      Serial.print("AWat");
      Serial.print("\t");
    }
    if (bitRead(data, 7) == 1) {
      Serial.print("MWat");
      Serial.print("\t");
    }
  Serial.println("");

  // split writing data when rst flag was active
  if (bitRead(EEPROM.read(address + 2), 3) == 1) {
    Serial.println("");
  }
  address -= 3;
  if (address < 10) {
    address = EEPROM.length() - 3;
  }
  }
}

// function clears whole EEPROM
void clrEEPROM() {
  static byte addressValue;
  for (int address = 0; address < EEPROM.length(); address++){
    addressValue = EEPROM.read(address);
    if (addressValue != 255) {
      EEPROM.write(address, 255);
    }
  }
}
